﻿
/// <summary>
/// 
///		============================================================
///		Enum Dice
///		
///		Este enum contem as ids dos dados modelados.
///		
///		by MMG
///		============================================================
///		
/// </summary>
public enum Dice
{
	D4,
	D6,
	D8,
	D10,
	D12,
	D20
}
