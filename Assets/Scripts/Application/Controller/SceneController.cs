﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		if(Application.loadedLevelName.Equals(Strings.SplashScreen))
		{
			//
			// It's the first scene,
			// prepare the application here.
			this.StartCoroutine(this.Load(Strings.MainMenu));
		}
	}
	
	public IEnumerator Load(string paramScene)
	{
		yield return new WaitForSeconds(1.5f);
		//
		//Calling the paramScene scene.
		Application.LoadLevel(paramScene);
	}
}
