﻿using UnityEngine;
using System.Collections;

public class ApplicationSetup : MonoBehaviour
{

	void Awake()
	{
		//
		// Previne que a aplicação seja interrompida 
		// para que a aplicação entre em modo de 
		// deascanso de tela.
		//
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
