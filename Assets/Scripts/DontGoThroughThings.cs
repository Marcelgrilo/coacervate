﻿using UnityEngine;
using System.Collections;

public class DontGoThroughThings : MonoBehaviour
{
	public LayerMask LayerMask; //make sure we aren't in this layer 
	public float SkinWidth = 0.1f; //probably doesn't need to be changed 
	
	private float myMinimumExtent; 
	private float myPartialExtent; 
	private float mySqrtMinimumExtent; 
	private Vector3 myPreviousPosition; 
	private Rigidbody myRigidbodyComponent; 
	private Collider myColliderComponent;

	//initialize values 
	void Awake ()
	{ 
		myColliderComponent = GetComponent<Collider> ();
		myRigidbodyComponent = GetComponent<Rigidbody> ();
		myPreviousPosition = myRigidbodyComponent.position; 
		myMinimumExtent = Mathf.Min (Mathf.Min (myColliderComponent.bounds.extents.x, myColliderComponent.bounds.extents.y), myColliderComponent.bounds.extents.z); 
		myPartialExtent = myMinimumExtent * (1.0f - SkinWidth); 
		mySqrtMinimumExtent = myMinimumExtent * myMinimumExtent;  
	} 
	
	void FixedUpdate ()
	{ 
		//have we moved more than our minimum extent? 
		Vector3 movementThisStep = myRigidbodyComponent.position - myPreviousPosition; 
		float movementSqrMagnitude = movementThisStep.sqrMagnitude;
		
		if (movementSqrMagnitude > mySqrtMinimumExtent) { 
			float movementMagnitude = Mathf.Sqrt (movementSqrMagnitude);
			RaycastHit hitInfo; 
			
			//check for obstructions we might have missed 
			if (Physics.Raycast (myPreviousPosition, movementThisStep, out hitInfo, movementMagnitude, LayerMask.value)) 
				myRigidbodyComponent.position = hitInfo.point - (movementThisStep / movementMagnitude) * myPartialExtent; 
		} 
		
		myPreviousPosition = myRigidbodyComponent.position; 
	}
}

