﻿using UnityEngine;
using UnityEngine.UI;

public class NumberField : MonoBehaviour
{

	Button[] myButtons;

	Text myInfoText;

	int myCurrentValue;

	int myMinValue = 0;
	int myMaxValue = 10;


	public int Value {
		get{ return myCurrentValue;}
	}

	// Use this for initialization
	void Start ()
	{
		myCurrentValue = 0;

		myButtons = transform.GetComponentsInChildren<Button> ();
		myButtons [0].onClick.AddListener (Increment);
		myButtons [1].onClick.AddListener (Decrement);
	
		myInfoText = GetComponentInChildren<Text> ();
		myInfoText.text = "0";
	}

	void Increment ()
	{
		if (myCurrentValue < myMaxValue) {
			myCurrentValue++;
			myInfoText.text = myCurrentValue.ToString ();
		}

	}

	void Decrement ()
	{
		if (myCurrentValue > myMinValue) {
			myCurrentValue--;
			myInfoText.text = myCurrentValue.ToString ();
		}
	}


}
