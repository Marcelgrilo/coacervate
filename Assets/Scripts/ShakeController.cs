﻿using UnityEngine;
using System.Text;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class ShakeController : MonoBehaviour 
{
	
	public GUIText GuiTextPosition;
	
	/// <summary>
	/// Força para aplicar nos dados... vai de 0 a 100.
	/// </summary>
	public float ForceMultiplier;
	
//	private Vector3 lastAceleration;
	
	public GameObject [] dices;
	
	//private Vector3 ExplosionForce;
	void Start () 
	{
		//this.isShaking = false;
		Input.location.Start(1,1);
		
		Input.compass.enabled = true;
	//	lastAceleration = Input.acceleration;
		Input.gyro.enabled = true;
		//ExplosionForce = Vector3.zero;

		this.sbTest = new StringBuilder();
	}

	private StringBuilder sbTest;

	void Update () 
	{
#if UNITY_ANDROID
		this.sbTest.Remove(0,this.sbTest.Length);
		//Vector3 deltaAcceletation = Input.acceleration - lastAceleration;		

		//GuiTextPosition.text =	
	//	this.sbTest.Append("ACELERATION:\n\t");
	//	this.sbTest.Append(Input.acceleration.ToString());
	//	this.sbTest.Append("\ndeltaVelocity:\n\t");
	//	this.sbTest.Append((Input.acceleration * Time.deltaTime).ToString());
	//	this.sbTest.Append("\ndeltaPosition:\n\t");
	//	this.sbTest.Append((Input.acceleration * Time.deltaTime * Time.deltaTime).ToString());

	//	"\n\naccelerationOther:\n\t" + deltaAcceletation.ToString() +
	//	"\ndeltaVelocityOther:\n\t" + (deltaAcceletation * Time.deltaTime).ToString() +
	//	"\ndeltaPositionOther:\n\t" + (deltaAcceletation * Time.deltaTime * Time.deltaTime).ToString() +

	//this.sbTest.Append("\n\nCompassEnabled:\n\t");
	//	this.sbTest.Append(Input.compass.enabled);
	//	this.sbTest.Append("\nCompassRawVector:\n\t");
	//	this.sbTest.Append(Input.compass.rawVector.ToString());
	//	this.sbTest.Append("\nCompassMagneticHeading:\n\t");
	//	this.sbTest.Append(Input.compass.magneticHeading);

	//	this.sbTest.Append("\n\nGyroEnabld:\n\t");
	//	this.sbTest.Append(Input.gyro.enabled);
	//	this.sbTest.Append("\nGyroAttitude:\n\t");
	//	this.sbTest.Append(Input.gyro.attitude.ToString());
	//	this.sbTest.Append("\nGyroRotationRate:\n\t");
	//	this.sbTest.Append(Input.gyro.rotationRate.ToString());
	//	this.sbTest.Append("\nGyroRotationRateUnbiased:\n\t");
	//	this.sbTest.Append(Input.gyro.rotationRateUnbiased.ToString());
	//	this.sbTest.Append("\nGyroUpdateInterval:\n\t");
	//	this.sbTest.Append(Input.gyro.updateInterval.ToString());
	//	this.sbTest.Append("\nGyroAcceleration:\n\t");
	//	this.sbTest.Append(Input.gyro.userAcceleration.ToString());

		for(int i = 0; i < dices.Length; i++)
		{
			this.sbTest.Append("\n\nDice " + i);
			this.sbTest.Append(dices[i].transform.position.ToString());
		}
		GuiTextPosition.text = this.sbTest.ToString();
		//Vector3 position = (1000*deltaAcceletation*Time.deltaTime*Time.deltaTime);
		//this.transform.position = new Vector3(this.transform.position.x+position.x,this.transform.position.y+position.y,this.transform.position.z+position.z); 
		
		//lastAceleration = Input.acceleration;
		
		//Vector3 force = new Vector3( Input.gyro.userAcceleration.x, 0, Input.gyro.userAcceleration.y);
		
		////this.transform.Rotate(Input.gyro.rotationRate);
		////this.rigidbody.AddForce(force*80,ForceMode.Acceleration);
		
		//for(int i = 0; i < dices.Length; i++)
		//{
		//	//if(force*5)
			
		//	dices[i].rigidbody.AddForce((-0.5F)*force*intensity,ForceMode.Acceleration);
			
		//	//ExplosionForce;
		//	//dices[i].rigidbody.AddTorque(force,ForceMode.Impulse);
		//}
#endif			
		
	}
	
	void FixedUpdate () 
	{
		//this.rigidbody.angularVelocity = Vector3.zero;
		
#if UNITY_ANDROID

		Vector3 force = new Vector3(Input.gyro.userAcceleration.x, Input.gyro.userAcceleration.z * 1.5F, Input.gyro.userAcceleration.y);

		//this.transform.Rotate(Input.gyro.rotationRate);
		//this.rigidbody.AddForce(force*80,ForceMode.Acceleration);

		for(int i = 0; i < dices.Length; i++)
		{
			dices [i].GetComponent<Rigidbody>().AddForce(Mathf.Clamp((-this.ForceMultiplier * 0.25F), -150f,150f)* force , ForceMode.Force);
		}
		
#endif
		
	}
}
