﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
///		============================================================
///		Anti stuck.
///		
///		Este script faz com que o objeto adicione um pequeno torque 
///		neste corporigido para caso este objeto fique travado no chão 
///		ou em outros objetos.
///		
///		Este script faz com que este objeto, caso saia dos limites
///		da caixa, volte para uma posição dentro da caixa.
///		
///		Este script faz com que cada colisão deste objeto no chão 
///		adicione um torque aleatótio baseado na velocidade de queda 
///		do objeto para dar mais naturalidade ao movimento de queda 
///		do dado.
///		
///		by MMG
///		============================================================
///		
/// </summary>
public class AntiStuckController : MonoBehaviour
{

	/// <summary>
	/// Limite do chão
	/// </summary>
	//private static float FLOOR_LIMIT = -5.0F;
	/// <summary>
	/// 
	/// </summary>
	//private static float CEIL_LIMIT = 3.0F;
	/// <summary>
	/// 
	/// </summary>
	//private Vector3 v3InitialPosition;
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		//
		// Starts a corroutine because the update would make it all time...
		// 
		StartCoroutine (Shake ());
		//
		// Armazena a posição inicial do dado.
		//
	//	this.v3InitialPosition = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);
	}
	/// <summary>
	/// 
	/// </summary>
	void FixedUpdate ()
	{
//		//
//		// Se a posição y deste objeto for 
//		// menor que o limite inferior OU
//		// maior que o limite superior
//		//
//		if (this.transform.position.y < AntiStuckController.FLOOR_LIMIT || this.transform.position.y > AntiStuckController.CEIL_LIMIT) {
//			//
//			// Zera a velocidade deste objeto.
//			//
//			this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
//			//
//			// Reposiciona este objeto.
//			//
//			this.transform.position = this.v3InitialPosition;
//		}
	}
	/// <summary>
	/// Adiciona um pequeno torque para este objeto de 2 em 2 segundos.
	/// </summary>
	private IEnumerator Shake ()
	{
		//
		// Loop Infinito.
		//
		while (true) {
			//
			// Adiciona um torque randômico pequeno.
			//
			this.GetComponent<Rigidbody> ().AddTorque (Random.Range (-0.1f, 0.1f), Random.Range (-0.1f, 0.1f), Random.Range (-0.1f, 0.1f));
			//
			// Time to wait until the next torque to be called.
			//
			yield return new WaitForSeconds (2.0f);
		}
	}

	/// <summary>
	/// A cada colisão com um objeto.
	/// </summary>
	/// <param name="other">
	/// Que representa a instância do objeto ao qual este
	/// objeto está colidindo.
	/// </param>
	void OnCollisionEnter (Collision other)
	{
		//
		// Se a velocidade em y deste corpo rígido dfor maior que 0.5F
		//
		if (this.GetComponent<Rigidbody> ().velocity.y > 0.5F) {
			//
			// velocity recebe o módulo da velocidade vetorial.
			//
			float velocity = this.GetComponent<Rigidbody> ().velocity.magnitude;
			//
			// Gera um valor aleatório baseado no módulo da velocidade.
			//
			Vector3 randomv3 = new Vector3
										(
											velocity * Random.Range (-velocity, velocity), 
											velocity * Random.Range (-0.1f, 0.1f), 
											velocity * Random.Range (-velocity, velocity)
			);
			//
			// Adiciona torque a este corpo rígido qeuivalente ao vetor 
			// aleatório gerado acima.
			//
			this.GetComponent<Rigidbody> ().AddTorque (randomv3);
		}
	}
}
