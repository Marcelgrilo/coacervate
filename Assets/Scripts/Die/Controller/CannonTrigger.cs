﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Atira um objeto em uma determinada 
/// posicao usando uma determinada forca.
/// </summary>
public class CannonTrigger : MonoBehaviour
{

	#region StaticsAttributes

	/// <summary>
	/// Limite da forca.
	/// </summary>
	private static Vector2 ForceLimits = new Vector2(0.0f, 150.0f);

	#endregion StaticsAttributes

	#region Attributes

	/// <summary>
	/// A forca que sera aplicada ao projetil.
	/// </summary>
	private float fForce;

	#endregion Attributes

	#region Properties

	/// <summary>
	/// O alvo.
	/// </summary>
	public Transform Target;

	/// <summary>
	/// O projetil.
	/// </summary>
	public GameObject Projectile;

	/// <summary>
	/// Propriedade para configurar a forca.
	/// </summary>
	public float Force
	{
		get
		{
			//
			// Retorna a forca.
			//
			return this.fForce;
		}
		set
		{
			//
			// Se value for maior que o limite superior.
			//
			if(value > ForceLimits.y)
			{
				//
				// A forca recebe o limite superior.
				//
				this.fForce = ForceLimits.y;
			}
			//
			// Senao se value for menor que o limite inferior.
			//
			else if(value < ForceLimits.x)
			{
				//
				// A forca recebe o limite inferior.
				//
				this.fForce = ForceLimits.x;
			}
			//
			// Senao ( caso a value esteja dento do limite superior e inferior )
			//
			else
			{
				//
				// A forca recebe o valor.
				//
				this.fForce = value;
			}
		}
	}

	#endregion Properties

	#region Methods

	/// <summary>
	/// Atira o projetil na direção do alvo com a forca dada.
	/// </summary>
	public void Shoot()
	{
		//
		// Se o alvo ou o projetil ou o corpo rigido do projetil forem nulos.
		//
		if(Target == null || Projectile == null || Projectile.GetComponent<Rigidbody>() == null)
		{
			//
			// Retorna
			//
			return;
		}

		//
		// Posiciona o objeto na mesma posicao deste objeto.
		//
		this.Projectile.transform.position = this.transform.position;

		//
		// Mira o projetil para o alvo.
		//
		this.Projectile.transform.LookAt(this.Target);

		//
		// Atira o projetil usando a forca na direcao do alvo.
		//
		this.Projectile.GetComponent<Rigidbody>().AddForce(0.0f, 0.0f, fForce);
	}

	#endregion Mehtods

}