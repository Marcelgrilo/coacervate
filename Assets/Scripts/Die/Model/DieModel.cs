﻿using UnityEngine;
using System.Collections;
using System.Text;

/// <summary>
/// 
///		============================================================
///		DieModel
/// 
///		A classe modelo para os dados de:
///		4 lados, 6 lados, 8 lados, 10 lados, 12 lados e 20 lados.
/// 
///		Esta classe dado um modelo calcula qual face do dado está 
///		sob o chão e define qual a face voltada para o usuário.
/// 
///		by MMG
///		============================================================
/// 
/// </summary>
public class DieModel : MonoBehaviour
{
	#region Consts and Static Parameters

		/// <summary>
		///	The valid hitVector check margin.
		/// </summary>
		private static float fErrorMargin = 0.45F;

	#endregion Consts and Static Parameters

	#region Properties

		/// <summary>
		///	Valor do dado quando este tiver uma face virada para cima.
		/// </summary>
		public int DieValue;

		/// <summary>
		///	O tipo do dado.
		///		04 lados = Dices.D4,
		///		06 lados = Dices.D6,
		///		08 lados = Dices.D8,
		///		10 lados = Dices.D10,
		///		12 lados = Dices.D12 e
		///		20 lados = Dices.D20.
		/// </summary>
		public Dice DieType = Dice.D6;

		/// <summary>
		///	Devolve o valor dizendo se este objeto <see cref="DieModel"/> 
		///	está se movendo ou está parado.
		/// </summary>
		/// 
		/// <value>
		///	<c>true</c> se está movendo, senão, <c>false</c>.
		/// </value>
		public bool IsMoving {
				get {
						//
						// Pega o valor modular da velocidade e da velocidade angular, verifica se 
						// um deles é menor que 0.1F ( Valor considerado parado ) então retorna o
						// valor da operação booleana negado ( não está parado).
						//
						return !(
						(this.GetComponent<Rigidbody> ().velocity.sqrMagnitude < 0.1F) ||
								(this.GetComponent<Rigidbody> ().angularVelocity.sqrMagnitude < 0.1F)
					);
				}
		}

	#endregion Properties

	#region Attributes
		private Coroutine<int> coroutineLoopController;

		/// <summary>
		///	Vetor normalizado que vai do centro do objeto até o lado mais baixo localmente, 
		///	o qual é usado para determinar se o lado de um dado está para cima.
		/// </summary>
		private Vector3 v3DetectedFaceNormalized;

		/// <summary>
		///		Pega um valor para este objeto <see cref="DieModel"/> que indica
		///		se uma face está sendo detectada.
		/// </summary>
		/// 
		/// <value>
		///		retorna <c>true</c> se há uma face sendo tocada e armazena o vetor que vai do 
		///		centro do objeto até a face tocada; senão, <c>false</c>.
		/// </value>
		private bool IsFaceDetected {
				get {
						//
						// Cria um raio abaixo do objeto e com direção para cima.
						//
						Ray myRay = new Ray (this.transform.position + (new Vector3 (0, -2, 0) * this.transform.localScale.magnitude), Vector3.up * 0.1F);
						//
						// Inicializa o RaycastHit.
						//
						RaycastHit myHit = new RaycastHit ();
						//
						// Se o collider faz o raycast de myRay e é atingido.
						//
						if (this.GetComponent<Collider> ().Raycast (myRay, out myHit, 3 * this.transform.localScale.magnitude)) {
				
								//Debug.DrawRay(myRay.origin, myRay.direction, Color.green);
								//
								// Determina-se o vetor normalizado que vai do centro do dado
								// até a face que foi atingida pelo raycast.
								// Para isto, precisa-se usar a coordenada local. Cada lado de
								// cada dado terá um valor próprio para o vetor de coordenadas
								// (que sempre será o mesmo para um determionado tipo de dado).
								// Portanto deve-se converter o ponto de acerto para coordenada
								// local e normalizar o valor.
								//
								this.v3DetectedFaceNormalized = this.transform.InverseTransformPoint (myHit.point.x, myHit.point.y, myHit.point.z).normalized;				
								//
								// Retorna verdadeiro se houve colisão do raycast com uma face.
								//
								return true;
						}
						//
						// Nunca retornara falso 
						// (só se o raycast estiver errado ou o mesh collider estiver vazado).
						//
						return false;
				}
		}

	#endregion Attributes

	#region Methods


		/// <summary>
		///	Calcula o valor da face virada para cima no dado..
		/// </summary>
		private void CalculateFace ()
		{
				//if(DieType == Dice.D8)
				//{
				//	if(str.Length > 1)
				//	{
				//		str.Remove(0, str.Length - 1);
				//	}
				//	str.Append("getValue:\n\n");
				//}
				//
				//Reinicializa o valor encontrado para 0 ( indeterminado ).
				//
				this.DieValue = 0;		
				//
				// Valor usado para 
				//
				float myLastDelta = 1;
				//
				// Lado do dado iniciando como 1, usado para 
				// incrementar a cada verificação de face.
				//
				int mySideValue = 1;
				//
				// Vetor correspondente a posição do dado no chao.
				//
				Vector3 myFaceVector;
				//
				// Passa por todos os lados do dado checando se o lado tem um vetor correspondente a 
				// face válida e menor que o delta de x, y e z entre o vetor correspondente a posição 
				// do dado no chao(calculado por <see cref="DieModel.FaceVector(int)"/>) e o vetor
				// normalizado que vai do centro do objeto até o lado mais baixo(calculado em
				// <see cref="DieModel.IsFaceDetected()"/>). O lado que mais se aproximar será o 
				// representante do valor do dado.
				//
				do {
						//
						// Pega o vetor para o lado atual.
						//
						myFaceVector = this.FaceVector (mySideValue, DieType);
						//
						// Se o vetor da face for diferente de zero.
						//
						if (myFaceVector != Vector3.zero) {
								//if(DieType == Dice.D8)
								//{
								//	str.Append(v3DetectedFaceNormalized + " --=> " + myFaceVector + "\n");
								//}
								//
								// Verifica se os valores são válidos de acordo com a margem de erro 
								// <see cref="DieModel.fErrorMargin"/>.
								//
								if (
						this.ValidateValues (this.v3DetectedFaceNormalized.x, myFaceVector.x) &&
										this.ValidateValues (this.v3DetectedFaceNormalized.y, myFaceVector.y) &&
										this.ValidateValues (this.v3DetectedFaceNormalized.z, myFaceVector.z)
					) {
										//
										// Calcula o delta entre delta de x, y e z entre o vetor correspondente a posição
										// do dado no chao(calculado por <see cref="DieModel.FaceVector(int)"/>) e o vetor
										// normalizado que vai do centro do objeto até o lado mais baixo(calculado em
										// <see cref="DieModel.IsFaceDetected()"/>) para verificar se pode-se setar este 
										// lado como o lado correto.
										// Se mais de um lado estão dentro da margem, usa-se o valor que está mais próximo
										// ao lado correto calculado pelo delta.
										//
										float myCalculatedDelta = Mathf.Abs (this.v3DetectedFaceNormalized.x - myFaceVector.x) +
												Mathf.Abs (this.v3DetectedFaceNormalized.y - myFaceVector.y) +
												Mathf.Abs (this.v3DetectedFaceNormalized.z - myFaceVector.z);
										//
										// Se o valor atual calculado for mais distante do que o valor anterior.
										//
										if (myCalculatedDelta < myLastDelta) {
												//
												// Valor do dado recebe o novo valor.
												//
												this.DieValue = mySideValue;
												//
												// Menor valor de delta antigo recebe o atual.
												//
												myLastDelta = myCalculatedDelta;
										}
								}
						}
						//
						// Incrementa o lado do dado.
						//
						mySideValue++;
						//
						// Se o vetor de face for zero significa que todos os lados do dado já foram visitados
						// e o dado ja fora completamente percorrido.
						//
				} while(myFaceVector != Vector3.zero);
		}


		/// <summary>
		///	Primeira ação antes de inicializar o update.
		/// </summary>
		void Start ()
		{
				//
				// Inicializa valores para antes do início do Update.
				//
				this.DieValue = 0;
		
		}

		/// <summary>
		/// Método chamado quando este objeto é habilitado.
		/// </summary>
		void OnEnable ()
		{
				//
				// Inicializa o update otimizado que roda de 1 em 1 segundo.
				//
				this.coroutineLoopController = this.StartCoroutine<int> (this.CoUpdate ());
		}

		/// <summary>
		/// Método chamado quando este objeto é desabilitado.
		/// </summary>
		void OnDisable ()
		{
				//
				// Cancela a corrotina de looping.
				//
				this.coroutineLoopController.Cancel ();
		}

		/// <summary>
		///	Update otimizado desta instância.
		/// </summary>
		IEnumerator CoUpdate ()
		{
				//
				// Loop infinito
				//
				while (this.gameObject.activeSelf) {
						//
						// Se o objeto não está movendo ou se tem alguma face detectada.
						//
						if (!this.IsMoving && this.IsFaceDetected) {
								//
								// Recalcula o valor da face do dado.
								//
								this.CalculateFace ();
						}
						//
						// Espera 0.5 segundos e retorna.
						//
						yield return new WaitForSeconds (0.50F);
				}
		}

	#endregion Methods

	#region Static Methods

		/// <summary>
		///	Verifica se os valores estão dentro da margem de erro.
		/// </summary>
		/// 
		/// <param name="paramA">( float )</param>
		/// <param name="paramB">( float )</param>
		/// 
		/// <returns>
		///	<c>true</c> se os valores estão dentro da margem de acerto.
		///	<c>false</c> se os valores estão fora da margem de acerto.
		/// </returns>
		private bool ValidateValues (float paramA, float paramB)
		{
				//
				// Se 
				// paramA é maior que paramB menos a margem 
				// E
				// paramA é menor que paramB mais a margem 
				//
				if (paramA > (paramB - DieModel.fErrorMargin) && paramA < (paramB + DieModel.fErrorMargin)) {
						return true;
				} else {
						return false;
				}
		}

		/// <summary>
		///	Retorna o vetor relativo aos parâmetrs passados
		/// </summary>
		/// 
		/// <param name="paramSide"> ( int )lado do dado </param>
		/// <param name="paramDieType">( Dice ) tipo do dado</param>
		/// 
		/// <returns> 
		///	Um vetor reprenentando o valor para o lado da face dado
		///	</returns>
		private Vector3 FaceVector (int paramSide, Dice paramDieType)
		{
				//
				// Escolhe o tipo de dado.
				//
				switch (paramDieType) {
				case Dice.D4:
				//
				// Valores representando cada lado de um Dice.D4
				//
						switch (paramSide) {
						case 1:
								return FourSidedDieVectors.Face01;
						case 2:
								return FourSidedDieVectors.Face02;
						case 3:
								return FourSidedDieVectors.Face03;
						case 4:
								return FourSidedDieVectors.Face04;
						default:
								break;
						}
						break;
			
				case Dice.D6:
				//
				// Valores representando cada lado de um Dice.D6
				//
						switch (paramSide) {
						case 1:
								return SixSidedDieVectors.Face01;
						case 2:
								return SixSidedDieVectors.Face02;
						case 3:
								return SixSidedDieVectors.Face03;
						case 4:
								return SixSidedDieVectors.Face04;
						case 5:
								return SixSidedDieVectors.Face05;
						case 6:
								return SixSidedDieVectors.Face06;
						default:
								break;
						}
						break;
			
				case Dice.D8:
				//
				// Valores representando cada lado de um Dice.D8
				//
						switch (paramSide) {
						case 01:
								return EightSidedDieVectors.Face01;
						case 02:
								return EightSidedDieVectors.Face02;
						case 03:
								return EightSidedDieVectors.Face03;
						case 04:
								return EightSidedDieVectors.Face04;
						case 05:
								return EightSidedDieVectors.Face05;
						case 06:
								return EightSidedDieVectors.Face06;
						case 07:
								return EightSidedDieVectors.Face07;
						case 08:
								return EightSidedDieVectors.Face08;
						default:
								break;
						}
						break;
			
				case Dice.D10:
				//
				// Valores representando cada lado de um Dice.D10
				//
						switch (paramSide) {
						case 01:
								return TenSidedDieVectors.Face01;
						case 02:
								return TenSidedDieVectors.Face02;
						case 03:
								return TenSidedDieVectors.Face03;
						case 04:
								return TenSidedDieVectors.Face04;
						case 05:
								return TenSidedDieVectors.Face05;
						case 06:
								return TenSidedDieVectors.Face06;
						case 07:
								return TenSidedDieVectors.Face07;
						case 08:
								return TenSidedDieVectors.Face08;
						case 09:
								return TenSidedDieVectors.Face09;
						case 10:
								return TenSidedDieVectors.Face10;
						default:
								break;
						}
						break;
			
				case Dice.D12:
				//
				// Valores representando cada lado de um Dice.D12
				//
						switch (paramSide) {
						case 01:
								return TwelveSidedDieVectors.Face01;
						case 02:
								return TwelveSidedDieVectors.Face02;
						case 03:
								return TwelveSidedDieVectors.Face03;
						case 04:
								return TwelveSidedDieVectors.Face04;
						case 05:
								return TwelveSidedDieVectors.Face05;
						case 06:
								return TwelveSidedDieVectors.Face06;
						case 07:
								return TwelveSidedDieVectors.Face07;
						case 08:
								return TwelveSidedDieVectors.Face08;
						case 09:
								return TwelveSidedDieVectors.Face09;
						case 10:
								return TwelveSidedDieVectors.Face10;
						case 11:
								return TwelveSidedDieVectors.Face11;
						case 12:
								return TwelveSidedDieVectors.Face12;
						default:
								break;
						}
						break;
			
				case Dice.D20:
				//
				// Valores representando cada lado de um Dice.D20
				//
						switch (paramSide) {
						case 01:
								return TwentySidedDieVectors.Face01;
						case 02:
								return TwentySidedDieVectors.Face02;
						case 03:
								return TwentySidedDieVectors.Face03;
						case 04:
								return TwentySidedDieVectors.Face04;
						case 05:
								return TwentySidedDieVectors.Face05;
						case 06:
								return TwentySidedDieVectors.Face06;
						case 07:
								return TwentySidedDieVectors.Face07;
						case 08:
								return TwentySidedDieVectors.Face08;
						case 09:
								return TwentySidedDieVectors.Face09;
						case 10:
								return TwentySidedDieVectors.Face10;
						case 11:
								return TwentySidedDieVectors.Face11;
						case 12:
								return TwentySidedDieVectors.Face12;
						case 13:
								return TwentySidedDieVectors.Face13;
						case 14:
								return TwentySidedDieVectors.Face14;
						case 15:
								return TwentySidedDieVectors.Face15;
						case 16:
								return TwentySidedDieVectors.Face16;
						case 17:
								return TwentySidedDieVectors.Face17;
						case 18:
								return TwentySidedDieVectors.Face18;
						case 19:
								return TwentySidedDieVectors.Face19;
						case 20:
								return TwentySidedDieVectors.Face20;
						default:
								break;
						}
						break;
			
				default:
						break;
				}
		
				return Vector3.zero;
		}

	#endregion Static Methods

}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de quatro lados;
/// </summary>
public static class FourSidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (0.0F, -1.0F, 0.0F);
		public static Vector3 Face02 = new Vector3 (0.0F, 0.3F, -0.9F);
		public static Vector3 Face03 = new Vector3 (0.8F, 0.3F, 0.5F);
		public static Vector3 Face04 = new Vector3 (-0.8F, 0.4F, 0.5F);
}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de seis lados;
/// </summary>
public static class SixSidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (0.0F, 0.0F, -1.0F);
		public static Vector3 Face02 = new Vector3 (0.0F, 1.0F, 0.0F);
		public static Vector3 Face03 = new Vector3 (1.0F, 0.0F, 0.0F);
		public static Vector3 Face04 = new Vector3 (-1.0F, 0.0F, 0.0F);
		public static Vector3 Face05 = new Vector3 (0.0F, -1.0F, 0.0F);
		public static Vector3 Face06 = new Vector3 (0.0F, 0.0F, 1.0F);
}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de oito lados;
/// </summary>
public static class EightSidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (-0.6F, -0.6F, 0.6F);
		public static Vector3 Face02 = new Vector3 (0.6F, 0.6F, 0.6F);
		public static Vector3 Face03 = new Vector3 (0.6F, -0.6F, 0.6F);
		public static Vector3 Face04 = new Vector3 (-0.6F, 0.6F, 0.6F);
		public static Vector3 Face05 = new Vector3 (0.6F, -0.6F, -0.6F);
		public static Vector3 Face06 = new Vector3 (-0.6F, 0.6F, -0.6F);
		public static Vector3 Face07 = new Vector3 (-0.6F, -0.6F, -0.6F);
		public static Vector3 Face08 = new Vector3 (0.6F, 0.6F, -0.6F);
}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de dez lados;
/// </summary>
public static class TenSidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (-0.4F, 0.7F, -0.7F);
		public static Vector3 Face02 = new Vector3 (-0.4F, -0.7F, 0.7F);
		public static Vector3 Face03 = new Vector3 (0.7F, -0.2F, -0.7F);
		public static Vector3 Face04 = new Vector3 (0.0F, 0.7F, 0.7F);
		public static Vector3 Face05 = new Vector3 (0.0F, -0.7F, -0.7F);
		public static Vector3 Face06 = new Vector3 (-0.7F, 0.2F, 0.7F);
		public static Vector3 Face07 = new Vector3 (0.4F, 0.7F, -0.7F);
		public static Vector3 Face08 = new Vector3 (0.4F, -0.7F, 0.7F);
		public static Vector3 Face09 = new Vector3 (-0.7F, -0.2F, -0.7F);
		public static Vector3 Face10 = new Vector3 (0.7F, 0.2F, 0.7F);
}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de doze lados;
/// </summary>
public static class TwelveSidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (0.0F, -0.5F, -0.9F);
		public static Vector3 Face02 = new Vector3 (-0.5F, -0.9F, 0.0F);
		public static Vector3 Face03 = new Vector3 (0.9F, 0.0F, 0.5F);
		public static Vector3 Face04 = new Vector3 (0.5F, -0.9F, 0.0F);
		public static Vector3 Face05 = new Vector3 (0.0F, 0.5F, -0.9F);
		public static Vector3 Face06 = new Vector3 (0.9F, 0.0F, -0.5F);
		public static Vector3 Face07 = new Vector3 (-0.9F, 0.0F, 0.5F);
		public static Vector3 Face08 = new Vector3 (0.0F, -0.5F, 0.9F);
		public static Vector3 Face09 = new Vector3 (-0.5F, 0.9F, 0.0F);
		public static Vector3 Face10 = new Vector3 (-0.9F, 0.0F, -0.5F);
		public static Vector3 Face11 = new Vector3 (0.5F, 0.9F, 0.0F);
		public static Vector3 Face12 = new Vector3 (0.0F, 0.5F, 0.9F);
}

/// <summary>
/// Classe estática para guardar os valores das faces 
/// do dado de vinte lados;
/// </summary>
public static class TwentySidedDieVectors
{
		public static Vector3 Face01 = new Vector3 (-0.2F, 0.6F, -0.8F);
		public static Vector3 Face02 = new Vector3 (0.3F, -0.9F, 0.2F);
		public static Vector3 Face03 = new Vector3 (0.3F, 0.9F, 0.2F);
		public static Vector3 Face04 = new Vector3 (-0.8F, -0.6F, 0.2F);
		public static Vector3 Face05 = new Vector3 (-0.2F, -0.6F, -0.8F);
		public static Vector3 Face06 = new Vector3 (-0.5F, 0.4F, 0.8F);
		public static Vector3 Face07 = new Vector3 (0.5F, 0.4F, -0.8F);
		public static Vector3 Face08 = new Vector3 (0.6F, 0.0F, 0.8F);
		public static Vector3 Face09 = new Vector3 (-0.8F, 0.6F, 0.2F);
		public static Vector3 Face10 = new Vector3 (1.0F, 0.0F, 0.2F);
		public static Vector3 Face11 = new Vector3 (-1.0F, 0.0F, -0.2F);
		public static Vector3 Face12 = new Vector3 (0.8F, -0.6F, -0.2F);
		public static Vector3 Face13 = new Vector3 (-0.6F, 0.0F, -0.8F);
		public static Vector3 Face14 = new Vector3 (-0.5F, -0.4F, 0.8F);
		public static Vector3 Face15 = new Vector3 (0.5F, 0.4F, -0.8F);
		public static Vector3 Face16 = new Vector3 (0.2F, 0.6F, 0.8F);
		public static Vector3 Face17 = new Vector3 (0.8F, 0.6F, -0.2F);
		public static Vector3 Face18 = new Vector3 (-0.3F, -0.9F, -0.2F);
		public static Vector3 Face19 = new Vector3 (-0.3F, 0.9F, -0.2F);
		public static Vector3 Face20 = new Vector3 (0.2F, -0.6F, 0.8F);
}