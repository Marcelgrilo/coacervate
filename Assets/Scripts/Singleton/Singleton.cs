﻿using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	/// <summary>
	/// The static _instance that will be mantained by the singleton pattern.
	/// </summary>
	private static T instance;

	/// <summary>
	/// The _lock object that will aways lock the cals to the instance on the main thread.
	/// </summary>
	private static object _lock = new object();

	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <value>The instance.</value>
	public static T Instance
	{
		get
		{
			// in case that the application is quitting  it will return null.
			if (applicationIsQuitting) 
			{
				Debug.LogWarning("[Singleton] Instance '"+ typeof(T) +
				                 "' already destroyed on application quit." +
				                 " Won't create again - returning null.");
				return null;
			}

			// locks these block of commands.
			lock(_lock)
			{
				// if the singleton instance is null.
				if (instance == null)
				{
					// it will find for the frst instance of this type.
					instance = (T) FindObjectOfType(typeof(T));

					// if the size of the objects is higher than 1.
					if ( FindObjectsOfType(typeof(T)).Length > 1 )
					{
						// it cannot be true.
						//Debug.LogError("[Singleton] Something went really wrong " +
						//               " - there should never be more than 1 singleton!" +
						//               " Reopenning the scene might fix it.");
						// returns the first instance found
						return instance;
					}

					// if the instance is null after the search it will be instantiated.
					if (instance == null)
					{
						GameObject singleton = new GameObject();
						instance = singleton.AddComponent<T>();
						singleton.name = "[singleton] "+ typeof(T).ToString();

						// it will not be destroyed on loaded scenes.
						DontDestroyOnLoad(singleton);
						
						//Debug.Log("[Singleton] An instance of " + typeof(T) + 
						//          " is needed in the scene, so '" + singleton +
						//          "' was created with DontDestroyOnLoad.");
					}
					// if the instance isn't null, meaning that it found an instance on the search.
					else
					{
//						Debug.Log("[Singleton] Using instance already created: " +
//						          instance.gameObject.name);
					}
				}

				return instance;
			}
		}
	}

	/// <summary>
	/// The application is quitting.
	/// </summary>
	private static bool applicationIsQuitting = false;
	/// <summary>
	/// When Unity quits, it destroys objects in a random order.
	/// In principle, a Singleton is only destroyed when application quits.
	/// If any script calls Instance after it have been destroyed, 
	///   it will create a buggy ghost object that will stay on the Editor scene
	///   even after stopping playing the Application. Really bad!
	/// So, this was made to be sure we're not creating that buggy ghost object.
	/// </summary>
	public void OnDestroy () 
	{
		applicationIsQuitting = true;
	}
}