﻿using UnityEngine;
using System.Collections;

/// <summary>
///		============================================================
///		CameraRotator
///		
///		Script para rotacionar uma câmera de fundo aleatóriamente.
///		
///		by MMG
///		============================================================
/// </summary>
public class CameraRotator : MonoBehaviour 
{
	/// <summary>
	/// O quaternion que armazena o prózimo valor para a rotação.
	/// </summary>
	private Quaternion quatRotationDirection;

	/// <summary>
	/// Método Start que roda antes do primeiro frame ser chamado.
	/// </summary>
	void Start()
	{
		//
		// Inicializa quatRotationDirection com identidade.
		//
		this.quatRotationDirection = Quaternion.identity;
		//
		// Inicializa o loop Randômico.
		//
		this.StartCoroutine<int>(RandomUpdate());
	}

	/// <summary>
	/// Método que roda infinitamente e é chamado aleatóriamente 
	/// em um intervalo de tempo predeterminado.
	/// </summary>
	/// <returns>
	///		new WaitForSeconds(Random.Range(3.0F,6.0F)) - que faz 
	///		com que a próxima chamada da função aconteça entre 
	///		3 e 6 segundos.
	/// </returns>
	IEnumerator RandomUpdate()
	{
		//
		// Loop infinito
		//
		while(true)
		{
			//
			// Randomiza uma rotação e armazena em quatRotationDirection.
			//
			this.quatRotationDirection = Random.rotation;
			//
			// Randomiza um valor entre 3 e 6 segundos.
			//
			yield return new WaitForSeconds(Random.Range(3.0F,6.0F));
		}
	}

	/// <summary>
	/// LateUpdate usado geralmente para movimentação de câmeras.
	/// </summary>
	void LateUpdate()
	{
		//
		// Chama o método Slerp de quaternion para ir de pouco em pouco,
		// intermolando a rotação atual até a roração gerada aleatóriamente
		// no RandomUpdate.
		//
		this.transform.rotation = Quaternion.Slerp(this.transform.rotation, this.quatRotationDirection, Time.deltaTime * 0.05f);
	}
}
